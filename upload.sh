#!/bin/sh

assets=""

for x in *.pkg.tar.zst; do
  curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${x}" "${ASSETS_URL}/${x}"

  assets+=",{\"name\":\"${x}\",\"url\":\"${ASSETS_URL}/${x}\"}"
done

release-cli create \
  --name "$CI_COMMIT_TAG" \
  --tag-name "$CI_COMMIT_TAG" \
  --description "$CI_PROJECT_NAME Arch Linux AUR Package" \
  --assets-link "[${assets#,}]"
